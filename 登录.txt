<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:id="@+id/CL"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <ImageView
        android:layout_width="39dp"
        android:layout_height="34dp"
        app:srcCompat="@drawable/ic_launcher_background"
        tools:layout_editor_absoluteX="16dp"
        tools:layout_editor_absoluteY="16dp" />

    <TextView
        android:id="@+id/textView"
        android:layout_width="414dp"
        android:layout_height="148dp"
        android:gravity="center"
        android:text="品牌banner"
        android:background="@android:color/holo_blue_bright"
        android:textColor="@android:color/black"
        app:layout_constraintStart_toStartOf="parent"
        tools:layout_editor_absoluteY="1dp" />

    <ImageView
        android:id="@+id/imageView2"
        android:layout_width="60dp"
        android:layout_height="45dp"
        app:srcCompat="@drawable/ic_launcher_foreground"
        tools:layout_editor_absoluteX="350dp"
        tools:layout_editor_absoluteY="1dp" />

    <TextView
        android:id="@+id/textView3"
        android:layout_width="109dp"
        android:layout_height="26dp"
        android:text="代表你已经同意"
        tools:layout_editor_absoluteX="101dp"
        tools:layout_editor_absoluteY="595dp" />

    <Button
        android:id="@+id/button"
        android:layout_width="403dp"
        android:layout_height="33dp"
        android:textColor="@android:color/white"
        android:text="Button"
        android:background="@android:color/holo_blue_dark"
        tools:layout_editor_absoluteX="7dp"
        tools:layout_editor_absoluteY="319dp" />

    <TextView
        android:id="@+id/textView4"
        android:layout_width="166dp"
        android:layout_height="31dp"
        android:text="手机登录/注册"
        tools:layout_editor_absoluteX="125dp"
        tools:layout_editor_absoluteY="382dp" />

    <TextView
        android:id="@+id/textView5"
        android:layout_width="162dp"
        android:layout_height="26dp"
        android:text="用户协议、隐私政策"
        app:layout_constraintStart_toEndOf="@+id/textView3"
        tools:layout_editor_absoluteY="595dp" />

</androidx.constraintlayout.widget.ConstraintLayout>