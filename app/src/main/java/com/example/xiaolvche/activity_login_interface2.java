package com.example.xiaolvche;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class activity_login_interface2 extends AppCompatActivity {
    Button denglu;
    Button fanhui;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_interface2);
        denglu = findViewById(R.id.denglu);
        fanhui = findViewById(R.id.fanhui);
        denglu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("==========", "onClick: click");
                Intent intent = new Intent();
                //intent.setClass(MainActivity.this,activity_fee_scheduling.class);
                intent.setClass(activity_login_interface2.this, activity_rent_main.class);
                startActivity(intent);
            }
        });
        fanhui.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("==========", "onClick: click");
                Intent intent = new Intent();
                //intent.setClass(MainActivity.this,activity_fee_scheduling.class);
                intent.setClass(activity_login_interface2.this,Login_activity.class);
                intent.setClass(activity_login_interface2.this,activity_transport_process.class);
                startActivity(intent);
            }
        });

    }//
     /*protected void onCreate(Bundle savedInstanceState) {
     super.onCreate(savedInstanceState);
     setContentView(R.layout.activity_login_interface2);


 }*/
}