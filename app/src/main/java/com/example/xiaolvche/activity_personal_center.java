package com.example.xiaolvche;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class activity_personal_center extends AppCompatActivity {
    //按钮声明
    private Button useguide;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_center);
        //界面跳转
        useguide = findViewById(R.id.mc_use_guide_value);
        useguide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Log.d("==========", "onClick: click");
                Intent intent=new Intent(activity_personal_center.this,activity_personal_center_useguide.class);
                startActivity(intent);
            }
        });

    }


}