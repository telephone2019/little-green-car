package com.example.xiaolvche;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class activity_user_registration extends AppCompatActivity {
    Button queding;
    Button quxiao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_registration);
        queding = findViewById(R.id.register_btn_sure);
        quxiao = findViewById(R.id.register_btn_cancel);
        queding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("==========", "onClick: click");
                Intent intent = new Intent();
                //intent.setClass(MainActivity.this,activity_fee_scheduling.class);
                intent.setClass(activity_user_registration.this, activity_login_interface2.class);
                startActivity(intent);
            }
        });
        quxiao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("==========", "onClick: click");
                Intent intent = new Intent();
                //intent.setClass(MainActivity.this,activity_fee_scheduling.class);
                intent.setClass(activity_user_registration.this,Login_activity.class);
                intent.setClass(activity_user_registration.this,activity_login_interface2.class);
                startActivity(intent);
            }
        });

    }//
     /*protected void onCreate(Bundle savedInstanceState) {
     super.onCreate(savedInstanceState);
     setContentView(R.layout.activity_login_interface2);//


 }*/
}