package com.example.xiaolvche;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import android.view.View.OnClickListener;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

public class activity_end_ride extends AppCompatActivity   {
    private ImageButton findDevices;
    private Button sharing,check;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_ride);//每次创建完XML，再写对应的类的时候，这句话不要忘记，否则会闪退

        Intent intent = getIntent();
        String cost = intent.getStringExtra(("cost"));
        String time = intent.getStringExtra(("time"));
        String distence = intent.getStringExtra(("distence"));
        String balance = intent.getStringExtra(("balance"));//接收参数
        TextView cost_t = findViewById(R.id.cost);
        TextView time_distence = findViewById(R.id.qixing);
        TextView balance_t = findViewById(R.id.balance);
        cost_t.setText("本次骑行花费："+cost);
        time_distence.setText("骑行时间："+time + " | "+"骑行距离:"+distence );
        balance_t.setText("使用余额:"+balance );

        Button re_home = findViewById(R.id.backhomepage);

        re_home.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                finish();  //直接关闭当前页面
                startActivity(new Intent(activity_end_ride.this,activity_rent_main.class));
            }
        });
    }//返回首页键，结束当前页，返回首页

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return onOptionsItemSelected(item);
        }
    }
}

